﻿using System;

namespace BAndC
{
    abstract class Player
    {
        /// <summary>
        /// Защищенный конструктор только для производных классов,
        /// пустой конструктор используется вместо конструктора по умолчанию(public)
        /// </summary>
        protected Player() { }

        /// <summary>
        /// Секретное значение
        /// </summary>
        protected static string SecretValue { get; set; }

        public abstract string Guess();

        /// <summary>
        /// Количество символов
        /// </summary>
        private static int bitness = ChooseBitness();

        /// <summary>
        /// Метод устанавливает количество элементов в секрете
        /// </summary>
        /// <returns></returns>
        private static int ChooseBitness()
        {
            Console.WriteLine("Insert MAX Bitness:");

            var result = default(int);

            while (!int.TryParse(Console.ReadLine(), out result)) 
            {
                ErrorMsg2();
                continue;
            }

            if (result > 10)
            {
                result = 10;
                Console.WriteLine($"Too big number, bit'll be:\n{result}");
            } 

            Console.WriteLine($"Max bit is {result}\n");
            
            return result;
        }

        public int bit = bitness;

        /// <summary>
        /// Сообщение об ошибке некорректного ввода
        /// </summary>
        protected static void ErrorMsg2()
        {
            Console.WriteLine("Incorrect input, try number from 0 to 9");
        }

        /// <summary>
        /// Сообщение об ошибке ввода неуникальных значений
        /// </summary>
        protected static void ErrorMsg1()
        {
            Console.WriteLine("Incorrect input, use uniq values");
        }

        /// <summary>
        /// Метод сравнивает значение с секретом и возвращает правду или ложь
        /// </summary>
        /// <param name="значение, которое сравнивается с секретом"></param>
        /// <returns>если значения совпадают - возвращает true</returns>
        public BAC Comparer(string guess)
        {
            var bac = new BAC();

            if (guess == string.Empty) return bac;

            for (int i = 0; i < bitness; i++)
            {
                for (int j = 0; j < bitness; j++)
                {
                    if (guess[i] == SecretValue[j] && i == j) bac.Bulls++;
                    if (guess[i] == SecretValue[j] && i != j) bac.Cows++;
                }
            }

            return bac;
        }

    }
}
