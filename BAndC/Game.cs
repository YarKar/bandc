﻿using System;


namespace BAndC
{
    class Game 
    {
        /// <summary>
        /// Метод предлагает игрокам бросить кости
        /// </summary>
        /// <returns>Возвращает true, если у игрока выпало большее число, чем у AI </returns>
        private static string WhoIsFirst()
        {
            var dice = new Random();

            var playerResult = default(int);
            var aiResult = default(int);

            do
            {
                Console.WriteLine("Press any key to roll the dice");
                Console.ReadLine();

                playerResult = dice.Next(1, 7);
                aiResult = dice.Next(1, 7);

                Console.WriteLine($"{playerResult}");
                Console.WriteLine($"AI : {aiResult}");
            }
            while (playerResult == aiResult);

            if (playerResult > aiResult)
            {
                Console.WriteLine("Player guess secret");
                return "gamer";
            }
            else
            {
                Console.WriteLine("AI guess secret");
                return "ai";
            }
        }

        /// <summary>
        /// Игровой цикл
        /// </summary>
        public Game()
        {
            var pl = new Gamer();

            var ai = new AI();

            var guess = string.Empty;

            var triesCouter = default(int);

            if (WhoIsFirst() == "gamer")
            {
                pl.SetSecret();

                do
                {
                    guess = ai.Guess();

                    if (guess == string.Empty) continue;

                    triesCouter++;

                    Console.WriteLine($"{ai.Name} Guess is " + guess + "\n");
                }
                while (pl.Comparer(guess).Bulls != pl.bit)
                ;

                Console.WriteLine($"AI used {triesCouter} tries");
            }
            else
            {
                ai.SetSecret();

                Console.WriteLine("AI guessed it's secret");

                Console.WriteLine($"Player Guess is {guess}\n");

                do
                {
                    triesCouter++;

                    guess = pl.Guess();

                    Console.WriteLine($"{pl.Name} Guess is " + guess + "\n" + ai.Comparer(guess) + "\n");
                }
                while (ai.Comparer(guess).Bulls != ai.bit)
               ;

                Console.WriteLine($"Player used { triesCouter} tries");
            }

        }
    }
}
