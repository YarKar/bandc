﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAndC
{
    class BAC
    {
        public int Bulls { get; set; }
        public int Cows { get; set; }

        public override string ToString()
        {
            return $"{Bulls}b{Cows}c";
        }
    }
}
