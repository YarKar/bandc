﻿using System;
using System.Collections.Generic;

namespace BAndC
{
    class AI : Player
    {
        /// <summary>
        /// Поле используется для загадывания и угадывания секрета
        /// </summary>
        private Random rnd = new Random();

        private HashSet<string> UsedValues = new HashSet<string>();

        private string guess = string.Empty;

        /// <summary>
        /// Компьютер загадывает рандомное число
        /// </summary>
        /// <returns></returns>
        public string SetSecret()
        {
            var secret = string.Empty;

            while (secret.Length < bit)
            {
                var value = rnd.Next(0, 10);

                if (!secret.Contains(value.ToString())) secret += value;
            }

            return SecretValue = secret;
        }

        /// <summary>
        /// Метод угадывания компьютером
        /// содержит алгоритм рандомного угадывания
        /// </summary>
        /// <returns></returns>
        public override string Guess()
        {
            var temp = string.Empty;

            while (temp.Length < bit)
            {
                var value = rnd.Next(0, 10);

                if (!temp.Contains(value.ToString())) temp += value;
            }

            if (!UsedValues.Contains(temp))
            {
                UsedValues.Add(temp);
                guess = temp;
            }
            else guess = string.Empty;

            return guess;
        }

        /// <summary>
        /// Имя игрока
        /// </summary>
        public string Name { get { return "AI"; } set { Name = value; } }
    }
}
