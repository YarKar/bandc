﻿using System;
using System.Linq;

namespace BAndC
{
    class Gamer : Player
    {
        /// <summary>
        /// Игрок устанавливает секретное число
        /// </summary>
        /// <returns></returns>
        public string SetSecret()
        {
            Console.WriteLine("Insert your secret:");

            string secret = InsertValue();

            return SecretValue = secret;
        }

        /// <summary>
        /// Игрок угадывает секретное число
        /// </summary>
        /// <returns></returns>
        public override string Guess()
        {
            return InsertValue();
        }

        /// <summary>
        /// Проверка на правильность ввода
        /// </summary>
        /// <returns></returns>
        public string InsertValue()
        {
            string value = string.Empty;
            
            while (value.Length != bit)
            {
                value = string.Empty;

                var x = Console.ReadLine();

                if (!x.ToString().Any())
                {
                    ErrorMsg2();

                    continue;
                }

                else
                {
                    foreach (var item in x)
                    {
                        if (!value.Contains(item)) value += item;
                    }

                    continue;
                }
            }

            return value;
        }

        /// <summary>
        /// Имя игрока
        /// </summary>
        public string Name { get { return "Player"; } set { Name = value; } }
    }
}
